/**
 * @Author: Wang chunsheng  email:2192138785@qq.com
 * @Date:   2021-11-27 18:06:12
 * @Last Modified by:   Wang chunsheng  email:2192138785@qq.com
 * @Last Modified time: 2021-11-28 01:25:07
 */
import EleUploadImage from './EleUploadImage'

if (typeof window !== 'undefined' && window.Vue) {
  window.Vue.component(EleUploadImage.name, EleUploadImage)
}

export default EleUploadImage
