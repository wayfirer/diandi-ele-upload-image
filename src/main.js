/**
 * @Author: Wang chunsheng  email:2192138785@qq.com
 * @Date:   2021-11-27 18:06:12
 * @Last Modified by:   Wang chunsheng  email:2192138785@qq.com
 * @Last Modified time: 2021-11-28 01:25:22
 */
import Vue from 'vue'
import App from './App.vue'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import EleUploadImage from '../lib'

Vue.config.productionTip = false
Vue.use(ElementUI)
Vue.component(EleUploadImage.name, EleUploadImage)

new Vue({
  render: h => h(App)
}).$mount('#app')
