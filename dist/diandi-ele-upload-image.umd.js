(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory();
	else if(typeof define === 'function' && define.amd)
		define([], factory);
	else if(typeof exports === 'object')
		exports["diandi-ele-upload-image"] = factory();
	else
		root["diandi-ele-upload-image"] = factory();
})((typeof self !== 'undefined' ? self : this), function() {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "fb15");
/******/ })
/************************************************************************/
/******/ ({

/***/ "02b5":
/***/ (function(module, exports, __webpack_require__) {

// extracted by mini-css-extract-plugin

/***/ }),

/***/ "2133":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var _mini_css_extract_plugin_dist_loader_js_ref_6_oneOf_1_0_css_loader_index_js_ref_6_oneOf_1_1_vue_loader_lib_loaders_stylePostLoader_js_postcss_loader_src_index_js_ref_6_oneOf_1_2_cache_loader_dist_cjs_js_ref_0_0_vue_loader_lib_index_js_vue_loader_options_upload_2_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("86a6");
/* harmony import */ var _mini_css_extract_plugin_dist_loader_js_ref_6_oneOf_1_0_css_loader_index_js_ref_6_oneOf_1_1_vue_loader_lib_loaders_stylePostLoader_js_postcss_loader_src_index_js_ref_6_oneOf_1_2_cache_loader_dist_cjs_js_ref_0_0_vue_loader_lib_index_js_vue_loader_options_upload_2_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_mini_css_extract_plugin_dist_loader_js_ref_6_oneOf_1_0_css_loader_index_js_ref_6_oneOf_1_1_vue_loader_lib_loaders_stylePostLoader_js_postcss_loader_src_index_js_ref_6_oneOf_1_2_cache_loader_dist_cjs_js_ref_0_0_vue_loader_lib_index_js_vue_loader_options_upload_2_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* unused harmony reexport * */


/***/ }),

/***/ "86a6":
/***/ (function(module, exports, __webpack_require__) {

// extracted by mini-css-extract-plugin

/***/ }),

/***/ "87dd":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var _node_modules_mini_css_extract_plugin_dist_loader_js_ref_6_oneOf_1_0_node_modules_css_loader_index_js_ref_6_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_2_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_EleUploadImage_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("ae74");
/* harmony import */ var _node_modules_mini_css_extract_plugin_dist_loader_js_ref_6_oneOf_1_0_node_modules_css_loader_index_js_ref_6_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_2_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_EleUploadImage_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_mini_css_extract_plugin_dist_loader_js_ref_6_oneOf_1_0_node_modules_css_loader_index_js_ref_6_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_2_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_EleUploadImage_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* unused harmony reexport * */


/***/ }),

/***/ "ae74":
/***/ (function(module, exports, __webpack_require__) {

// extracted by mini-css-extract-plugin

/***/ }),

/***/ "b289":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var _mini_css_extract_plugin_dist_loader_js_ref_6_oneOf_1_0_css_loader_index_js_ref_6_oneOf_1_1_vue_loader_lib_loaders_stylePostLoader_js_postcss_loader_src_index_js_ref_6_oneOf_1_2_cache_loader_dist_cjs_js_ref_0_0_vue_loader_lib_index_js_vue_loader_options_EleGallery_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("02b5");
/* harmony import */ var _mini_css_extract_plugin_dist_loader_js_ref_6_oneOf_1_0_css_loader_index_js_ref_6_oneOf_1_1_vue_loader_lib_loaders_stylePostLoader_js_postcss_loader_src_index_js_ref_6_oneOf_1_2_cache_loader_dist_cjs_js_ref_0_0_vue_loader_lib_index_js_vue_loader_options_EleGallery_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_mini_css_extract_plugin_dist_loader_js_ref_6_oneOf_1_0_css_loader_index_js_ref_6_oneOf_1_1_vue_loader_lib_loaders_stylePostLoader_js_postcss_loader_src_index_js_ref_6_oneOf_1_2_cache_loader_dist_cjs_js_ref_0_0_vue_loader_lib_index_js_vue_loader_options_EleGallery_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* unused harmony reexport * */


/***/ }),

/***/ "f6fd":
/***/ (function(module, exports) {

// document.currentScript polyfill by Adam Miller

// MIT license

(function(document){
  var currentScript = "currentScript",
      scripts = document.getElementsByTagName('script'); // Live NodeList collection

  // If browser needs currentScript polyfill, add get currentScript() to the document object
  if (!(currentScript in document)) {
    Object.defineProperty(document, currentScript, {
      get: function(){

        // IE 6-10 supports script readyState
        // IE 10+ support stack trace
        try { throw new Error(); }
        catch (err) {

          // Find the second match for the "at" string to get file src url from stack.
          // Specifically works with the format of stack traces in IE.
          var i, res = ((/.*at [^\(]*\((.*):.+:.+\)$/ig).exec(err.stack) || [false])[1];

          // For all scripts on the page, if src matches or if ready state is interactive, return the script tag
          for(i in scripts){
            if(scripts[i].src == res || scripts[i].readyState == "interactive"){
              return scripts[i];
            }
          }

          // If no match, return null
          return null;
        }
      }
    });
  }
})(document);


/***/ }),

/***/ "fb15":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/@vue/cli-service/lib/commands/build/setPublicPath.js
// This file is imported into lib/wc client bundles.

if (typeof window !== 'undefined') {
  if (true) {
    __webpack_require__("f6fd")
  }

  var i
  if ((i = window.document.currentScript) && (i = i.src.match(/(.+\/)[^/]+\.js(\?.*)?$/))) {
    __webpack_require__.p = i[1] // eslint-disable-line
  }
}

// Indicate to webpack that this file can be concatenated
/* harmony default export */ var setPublicPath = (null);

// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js?{"cacheDirectory":"node_modules/.cache/vue-loader","cacheIdentifier":"3d2f78f8-vue-loader-template"}!./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/cache-loader/dist/cjs.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./lib/EleUploadImage.vue?vue&type=template&id=6de39e9f&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"ele-upload-image"},[(!_vm.crop)?_c('el-upload',{directives:[{name:"show",rawName:"v-show",value:(_vm.isShowUpload),expression:"isShowUpload"}],ref:"upload",style:({
      marginBottom: _vm.multiple && _vm.computedValues.length ? '20px' : '0px'
    }),attrs:{"accept":_vm.accept,"action":_vm.action,"before-upload":_vm.handleBeforeUpload,"data":_vm.data,"disabled":_vm.uploading,"drag":Boolean(_vm.drag),"headers":_vm.headers,"http-request":_vm.httpRequest,"limit":_vm.limit,"list-type":_vm.drag ? 'picture' : 'picture-card',"multiple":_vm.multiple,"name":_vm.name,"on-change":_vm.handleChange,"on-error":_vm.handleUploadError,"on-exceed":_vm.handleExceed,"on-success":_vm.handleUploadSuccess,"show-file-list":false,"withCredentials":_vm.withCredentials}},[_c('div',{directives:[{name:"loading",rawName:"v-loading",value:(_vm.uploading),expression:"uploading"}]},[(_vm.drag)?[_c('i',{staticClass:"el-icon-upload"}),_c('div',{staticClass:"el-upload__text"},[_vm._v("\n          将文件拖到此处，或\n          "),_c('em',[_vm._v("点击上传")])])]:[_c('div',{style:({
            width: _vm.size + 'px',
            height: _vm.size + 'px',
            lineHeight: _vm.size + 'px'
          })},[_c('i',{staticClass:"el-icon-plus"})])]],2),(_vm.showTip)?_c('div',{staticClass:"el-upload__tip",attrs:{"slot":"tip"},slot:"tip"},[_vm._v("\n      请上传\n      "),_c('b',{staticStyle:{"color":"#F56C6C"}},[_vm._v(_vm._s(_vm.fileType.length ? _vm.fileType.join("/") : "图片"))]),_vm._v("\n      格式文件\n      "),(_vm.fileSize)?[_vm._v("\n        ，且大小不超过\n        "),_c('b',{staticStyle:{"color":"#F56C6C"}},[_vm._v(_vm._s(_vm.fileSize)+"MB")])]:_vm._e()],2):_vm._e()]):_vm._e(),(_vm.crop)?_c('div',[_c('div',{directives:[{name:"show",rawName:"v-show",value:(_vm.isShowUpload),expression:"isShowUpload"}],staticClass:"el-upload el-upload--picture-card",staticStyle:{"margin-bottom":"20px"},style:({
        width: _vm.size + 'px',
        height: _vm.size + 'px',
        lineHeight: _vm.size + 'px'
      }),on:{"click":function($event){_vm.isShowCrop = true}}},[_c('i',{staticClass:"el-icon-plus avatar-uploader-icon"})]),(_vm.isShowCrop)?_c('cropper',{ref:"cropper",staticClass:"ele-upload-image--cropper",attrs:{"field":_vm.name,"headers":_vm.headers,"height":_vm.cropHeight,"noCircle":_vm.cropHeight !== _vm.cropWidth,"params":_vm.data,"url":_vm.action,"width":_vm.cropWidth,"withCredentials":_vm.withCredentials,"img-format":"png"},on:{"crop-success":_vm.handleCropSuccess,"crop-upload-fail":_vm.handleCropUploadError,"crop-upload-success":_vm.handleCropUploadSuccess,"src-file-set":_vm.handleSetFileSet},model:{value:(_vm.isShowCrop),callback:function ($$v) {_vm.isShowCrop=$$v},expression:"isShowCrop"}}):_vm._e()],1):_vm._e(),_c('ele-gallery',{attrs:{"lazy":_vm.lazy,"remove-fn":_vm.handleRemove,"size":_vm.size,"sliceSingle":true,"source":_vm.value,"thumbSuffix":_vm.thumbSuffix,"title":_vm.title}})],1)}
var staticRenderFns = []


// CONCATENATED MODULE: ./lib/EleUploadImage.vue?vue&type=template&id=6de39e9f&

// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js?{"cacheDirectory":"node_modules/.cache/vue-loader","cacheIdentifier":"3d2f78f8-vue-loader-template"}!./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/cache-loader/dist/cjs.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./node_modules/vue-image-crop-upload/upload-2.vue?vue&type=template&id=258c767c&
var upload_2vue_type_template_id_258c767c_render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{directives:[{name:"show",rawName:"v-show",value:(_vm.value),expression:"value"}],staticClass:"vue-image-crop-upload"},[_c('div',{staticClass:"vicp-wrap"},[_c('div',{staticClass:"vicp-close",on:{"click":_vm.off}},[_c('i',{staticClass:"vicp-icon4"})]),_c('div',{directives:[{name:"show",rawName:"v-show",value:(_vm.step == 1),expression:"step == 1"}],staticClass:"vicp-step1"},[_c('div',{staticClass:"vicp-drop-area",on:{"dragleave":_vm.preventDefault,"dragover":_vm.preventDefault,"dragenter":_vm.preventDefault,"click":_vm.handleClick,"drop":_vm.handleChange}},[_c('i',{directives:[{name:"show",rawName:"v-show",value:(_vm.loading != 1),expression:"loading != 1"}],staticClass:"vicp-icon1"},[_c('i',{staticClass:"vicp-icon1-arrow"}),_c('i',{staticClass:"vicp-icon1-body"}),_c('i',{staticClass:"vicp-icon1-bottom"})]),_c('span',{directives:[{name:"show",rawName:"v-show",value:(_vm.loading !== 1),expression:"loading !== 1"}],staticClass:"vicp-hint"},[_vm._v(_vm._s(_vm.lang.hint))]),_c('span',{directives:[{name:"show",rawName:"v-show",value:(!_vm.isSupported),expression:"!isSupported"}],staticClass:"vicp-no-supported-hint"},[_vm._v(_vm._s(_vm.lang.noSupported))]),(_vm.step == 1)?_c('input',{directives:[{name:"show",rawName:"v-show",value:(false),expression:"false"}],ref:"fileinput",attrs:{"type":"file"},on:{"change":_vm.handleChange}}):_vm._e()]),_c('div',{directives:[{name:"show",rawName:"v-show",value:(_vm.hasError),expression:"hasError"}],staticClass:"vicp-error"},[_c('i',{staticClass:"vicp-icon2"}),_vm._v(" "+_vm._s(_vm.errorMsg)+"\r\n\t\t\t")]),_c('div',{staticClass:"vicp-operate"},[_c('a',{on:{"click":_vm.off,"mousedown":_vm.ripple}},[_vm._v(_vm._s(_vm.lang.btn.off))])])]),(_vm.step == 2)?_c('div',{staticClass:"vicp-step2"},[_c('div',{staticClass:"vicp-crop"},[_c('div',{directives:[{name:"show",rawName:"v-show",value:(true),expression:"true"}],staticClass:"vicp-crop-left"},[_c('div',{staticClass:"vicp-img-container"},[_c('img',{ref:"img",staticClass:"vicp-img",style:(_vm.sourceImgStyle),attrs:{"src":_vm.sourceImgUrl,"draggable":"false"},on:{"drag":_vm.preventDefault,"dragstart":_vm.preventDefault,"dragend":_vm.preventDefault,"dragleave":_vm.preventDefault,"dragover":_vm.preventDefault,"dragenter":_vm.preventDefault,"drop":_vm.preventDefault,"touchstart":_vm.imgStartMove,"touchmove":_vm.imgMove,"touchend":_vm.createImg,"touchcancel":_vm.createImg,"mousedown":_vm.imgStartMove,"mousemove":_vm.imgMove,"mouseup":_vm.createImg,"mouseout":_vm.createImg}}),_c('div',{staticClass:"vicp-img-shade vicp-img-shade-1",style:(_vm.sourceImgShadeStyle)}),_c('div',{staticClass:"vicp-img-shade vicp-img-shade-2",style:(_vm.sourceImgShadeStyle)})]),_c('div',{staticClass:"vicp-range"},[_c('input',{attrs:{"type":"range","step":"1","min":"0","max":"100"},domProps:{"value":_vm.scale.range},on:{"mousemove":_vm.zoomChange}}),_c('i',{staticClass:"vicp-icon5",on:{"mousedown":_vm.startZoomSub,"mouseout":_vm.endZoomSub,"mouseup":_vm.endZoomSub}}),_c('i',{staticClass:"vicp-icon6",on:{"mousedown":_vm.startZoomAdd,"mouseout":_vm.endZoomAdd,"mouseup":_vm.endZoomAdd}})]),(!_vm.noRotate)?_c('div',{staticClass:"vicp-rotate"},[_c('i',{on:{"click":_vm.rotateImg}},[_vm._v("↻")])]):_vm._e()]),_c('div',{directives:[{name:"show",rawName:"v-show",value:(true),expression:"true"}],staticClass:"vicp-crop-right"},[_c('div',{staticClass:"vicp-preview"},[(!_vm.noSquare)?_c('div',{staticClass:"vicp-preview-item"},[_c('img',{style:(_vm.previewStyle),attrs:{"src":_vm.createImgUrl}}),_c('span',[_vm._v(_vm._s(_vm.lang.preview))])]):_vm._e(),(!_vm.noCircle)?_c('div',{staticClass:"vicp-preview-item vicp-preview-item-circle"},[_c('img',{style:(_vm.previewStyle),attrs:{"src":_vm.createImgUrl}}),_c('span',[_vm._v(_vm._s(_vm.lang.preview))])]):_vm._e()])])]),_c('div',{staticClass:"vicp-operate"},[_c('a',{on:{"click":function($event){return _vm.setStep(1)},"mousedown":_vm.ripple}},[_vm._v(_vm._s(_vm.lang.btn.back))]),_c('a',{staticClass:"vicp-operate-btn",on:{"click":_vm.prepareUpload,"mousedown":_vm.ripple}},[_vm._v(_vm._s(_vm.lang.btn.save))])])]):_vm._e(),(_vm.step == 3)?_c('div',{staticClass:"vicp-step3"},[_c('div',{staticClass:"vicp-upload"},[_c('span',{directives:[{name:"show",rawName:"v-show",value:(_vm.loading === 1),expression:"loading === 1"}],staticClass:"vicp-loading"},[_vm._v(_vm._s(_vm.lang.loading))]),_c('div',{staticClass:"vicp-progress-wrap"},[_c('span',{directives:[{name:"show",rawName:"v-show",value:(_vm.loading === 1),expression:"loading === 1"}],staticClass:"vicp-progress",style:(_vm.progressStyle)})]),_c('div',{directives:[{name:"show",rawName:"v-show",value:(_vm.hasError),expression:"hasError"}],staticClass:"vicp-error"},[_c('i',{staticClass:"vicp-icon2"}),_vm._v(" "+_vm._s(_vm.errorMsg)+"\r\n\t\t\t\t")]),_c('div',{directives:[{name:"show",rawName:"v-show",value:(_vm.loading === 2),expression:"loading === 2"}],staticClass:"vicp-success"},[_c('i',{staticClass:"vicp-icon3"}),_vm._v(" "+_vm._s(_vm.lang.success)+"\r\n\t\t\t\t")])]),_c('div',{staticClass:"vicp-operate"},[_c('a',{on:{"click":function($event){return _vm.setStep(2)},"mousedown":_vm.ripple}},[_vm._v(_vm._s(_vm.lang.btn.back))]),_c('a',{on:{"click":_vm.off,"mousedown":_vm.ripple}},[_vm._v(_vm._s(_vm.lang.btn.close))])])]):_vm._e(),_c('canvas',{directives:[{name:"show",rawName:"v-show",value:(false),expression:"false"}],ref:"canvas",attrs:{"width":_vm.width,"height":_vm.height}})])])}
var upload_2vue_type_template_id_258c767c_staticRenderFns = []


// CONCATENATED MODULE: ./node_modules/vue-image-crop-upload/upload-2.vue?vue&type=template&id=258c767c&

// CONCATENATED MODULE: ./node_modules/vue-image-crop-upload/utils/language.js
/* harmony default export */ var language = ({
	zh: {
		hint: '点击，或拖动图片至此处',
		loading: '正在上传……',
		noSupported: '浏览器不支持该功能，请使用IE10以上或其他现在浏览器！',
		success: '上传成功',
		fail: '图片上传失败',
		preview: '头像预览',
		btn: {
			off: '取消',
			close: '关闭',
			back: '上一步',
			save: '保存'
		},
		error: {
			onlyImg: '仅限图片格式',
			outOfSize: '单文件大小不能超过 ',
			lowestPx: '图片最低像素为（宽*高）：'
		}
	},
	'zh-tw': {
		hint: '點擊，或拖動圖片至此處',
		loading: '正在上傳……',
		noSupported: '瀏覽器不支持該功能，請使用IE10以上或其他現代瀏覽器！',
		success: '上傳成功',
		fail: '圖片上傳失敗',
		preview: '頭像預覽',
		btn: {
			off: '取消',
			close: '關閉',
			back: '上一步',
			save: '保存'
		},
		error: {
			onlyImg: '僅限圖片格式',
			outOfSize: '單文件大小不能超過 ',
			lowestPx: '圖片最低像素為（寬*高）：'
		}
	},
	en: {
		hint: 'Click or drag the file here to upload',
		loading: 'Uploading…',
		noSupported: 'Browser is not supported, please use IE10+ or other browsers',
		success: 'Upload success',
		fail: 'Upload failed',
		preview: 'Preview',
		btn: {
			off: 'Cancel',
			close: 'Close',
			back: 'Back',
			save: 'Save'
		},
		error: {
			onlyImg: 'Image only',
			outOfSize: 'Image exceeds size limit: ',
			lowestPx: 'Image\'s size is too low. Expected at least: '
		}
	},
	ro: {
		hint: 'Atinge sau trage fișierul aici',
		loading: 'Se încarcă',
		noSupported: 'Browser-ul tău nu suportă acest feature. Te rugăm încearcă cu alt browser.',
		success: 'S-a încărcat cu succes',
		fail: 'A apărut o problemă la încărcare',
		preview: 'Previzualizează',

		btn: {
			off: 'Anulează',
			close: 'Închide',
			back: 'Înapoi',
			save: 'Salvează'
		},

		error: {
			onlyImg: 'Doar imagini',
			outOfSize: 'Imaginea depășește limita de: ',
			loewstPx: 'Imaginea este prea mică; Minim: '
		}
	},
	ru: {
		hint: 'Нажмите, или перетащите файл в это окно',
		loading: 'Загружаю……',
		noSupported: 'Ваш браузер не поддерживается, пожалуйста, используйте IE10 + или другие браузеры',
		success: 'Загрузка выполнена успешно',
		fail: 'Ошибка загрузки',
		preview: 'Предпросмотр',
		btn: {
			off: 'Отменить',
			close: 'Закрыть',
			back: 'Назад',
			save: 'Сохранить'
		},
		error: {
			onlyImg: 'Только изображения',
			outOfSize: 'Изображение превышает предельный размер: ',
			lowestPx: 'Минимальный размер изображения: '
		}
	},
	'pt-br': {
		hint: 'Clique ou arraste o arquivo aqui para carregar',
		loading: 'Carregando…',
		noSupported: 'Browser não suportado, use o IE10+ ou outro browser',
		success: 'Sucesso ao carregar imagem',
		fail: 'Falha ao carregar imagem',
		preview: 'Pré-visualizar',
		btn: {
			off: 'Cancelar',
			close: 'Fechar',
			back: 'Voltar',
			save: 'Salvar'
		},
		error: {
			onlyImg: 'Apenas imagens',
			outOfSize: 'A imagem excede o limite de tamanho: ',
			lowestPx: 'O tamanho da imagem é muito pequeno. Tamanho mínimo: '
		}
	},
	fr: {
		hint: 'Cliquez ou glissez le fichier ici.',
		loading: 'Téléchargement…',
		noSupported: 'Votre navigateur n\'est pas supporté. Utilisez IE10 + ou un autre navigateur s\'il vous plaît.',
		success: 'Téléchargement réussit',
		fail: 'Téléchargement echoué',
		preview: 'Aperçu',
		btn: {
			off: 'Annuler',
			close: 'Fermer',
			back: 'Retour',
			save: 'Enregistrer'
		},
		error: {
			onlyImg: 'Image uniquement',
			outOfSize: 'L\'image sélectionnée dépasse la taille maximum: ',
			lowestPx: 'L\'image sélectionnée est trop petite. Dimensions attendues: '
		}
	},
	nl: {
		hint: 'Klik hier of sleep een afbeelding in dit vlak',
		loading: 'Uploaden…',
		noSupported: 'Je browser wordt helaas niet ondersteund. Gebruik IE10+ of een andere browser.',
		success: 'Upload succesvol',
		fail: 'Upload mislukt',
		preview: 'Voorbeeld',
		btn: {
			off: 'Annuleren',
			close: 'Sluiten',
			back: 'Terug',
			save: 'Opslaan'
		},
		error: {
			onlyImg: 'Alleen afbeeldingen',
			outOfSize: 'De afbeelding is groter dan: ',
			lowestPx: 'De afbeelding is te klein! Minimale afmetingen: '
		}
	},
	tr: {
		hint: 'Tıkla veya yüklemek istediğini buraya sürükle',
		loading: 'Yükleniyor…',
		noSupported: 'Tarayıcı desteklenmiyor, lütfen IE10+ veya farklı tarayıcı kullanın',
		success: 'Yükleme başarılı',
		fail: 'Yüklemede hata oluştu',
		preview: 'Önizle',
		btn: {
			off: 'İptal',
			close: 'Kapat',
			back: 'Geri',
			save: 'Kaydet'
		},
		error: {
			onlyImg: 'Sadece resim',
			outOfSize: 'Resim yükleme limitini aşıyor: ',
			lowestPx: 'Resmin boyutu çok küçük. En az olması gereken: '
		}
	},
	'es-MX': {
		hint: 'Selecciona o arrastra una imagen',
		loading: 'Subiendo...',
		noSupported: 'Tu navegador no es soportado, por favor usa IE10+ u otros navegadores más recientes',
		success: 'Subido exitosamente',
		fail: 'Sucedió un error',
		preview: 'Vista previa',
		btn: {
			off: 'Cancelar',
			close: 'Cerrar',
			back: 'Atrás',
			save: 'Guardar'
		},
		error: {
			onlyImg: 'Únicamente imágenes',
			outOfSize: 'La imagen excede el tamaño maximo:',
			lowestPx: 'La imagen es demasiado pequeña. Se espera por lo menos:'
		}
	},
	de: {
		hint: 'Klick hier oder zieh eine Datei hier rein zum Hochladen',
		loading: 'Hochladen…',
		noSupported: 'Browser wird nicht unterstützt, bitte verwende IE10+ oder andere Browser',
		success: 'Upload erfolgreich',
		fail: 'Upload fehlgeschlagen',
		preview: 'Vorschau',
		btn: {
			off: 'Abbrechen',
			close: 'Schließen',
			back: 'Zurück',
			save: 'Speichern'
		},
		error: {
			onlyImg: 'Nur Bilder',
			outOfSize: 'Das Bild ist zu groß: ',
			lowestPx: 'Das Bild ist zu klein. Mindestens: '
		}
	},
	ja: {
		hint: 'クリック・ドラッグしてファイルをアップロード',
		loading: 'アップロード中...',
		noSupported: 'このブラウザは対応されていません。IE10+かその他の主要ブラウザをお使いください。',
		success: 'アップロード成功',
		fail: 'アップロード失敗',
		preview: 'プレビュー',
		btn: {
			off: 'キャンセル',
			close: '閉じる',
			back: '戻る',
			save: '保存'
		},
		error: {
			onlyImg: '画像のみ',
			outOfSize: '画像サイズが上限を超えています。上限: ',
			lowestPx: '画像が小さすぎます。最小サイズ: '
		}
	},
	ua: {
		hint: 'Натисніть, або перетягніть файл в це вікно',
		loading: 'Завантажую……',
		noSupported: 'Ваш браузер не підтримується, будь ласка скористайтесь IE10 + або іншими браузерами',
		success: 'Завантаження виконано успішно',
		fail: 'Помилка завантаження',
		preview: 'Попередній перегляд',
		btn: {
			off: 'Відмінити',
			close: 'Закрити',
			back: 'Назад',
			save: 'Зберегти'
		},
		error: {
			onlyImg: 'Тільки зображення',
			outOfSize: 'Зображення перевищує граничний розмір: ',
			lowestPx: 'Мінімальний розмір зображення: '
		}
	},
	it: {
		hint: 'Clicca o trascina qui il file per caricarlo',
		loading: 'Caricamento del file…',
		noSupported: 'Browser non supportato, per favore usa IE10+ o un altro browser',
		success: 'Caricamento completato',
		fail: 'Caricamento fallito',
		preview: 'Anteprima',
		btn: {
			off: 'Annulla',
			close: 'Chiudi',
			back: 'Indietro',
			save: 'Salva'
		},
		error: {
			onlyImg: 'Sono accettate solo immagini',
			outOfSize: 'L\'immagine eccede i limiti di dimensione: ',
			lowestPx: 'L\'immagine è troppo piccola. Il requisito minimo è: '
		}
	},
	ar: {
		hint: 'اضغط أو اسحب الملف هنا للتحميل',
		loading: 'جاري التحميل...',
		noSupported: 'المتصفح غير مدعوم ، يرجى استخدام IE10 + أو متصفح أخر',
		success: 'تم التحميل بنجاح',
		fail: 'فشل التحميل',
		preview: 'معاينه',
		btn: {
			off: 'إلغاء',
			close: 'إغلاق',
			back: 'رجوع',
			save: 'حفظ'
		},
		error: {
			onlyImg: 'صور فقط',
			outOfSize: 'تتجاوز الصوره الحجم المحدد: ',
			lowestPx: 'حجم الصورة صغير جدا. من المتوقع على الأقل: '
		}
	},
	ug: {
		hint: 'مەزكۇر دائىرىنى چىكىپ رەسىم تاللاڭ ياكى رەسىمنى سۆرەپ ئەكىرىڭ',
		loading: 'يوللىنىۋاتىدۇ...',
		noSupported: 'تور كۆرگۈچ بۇ ئىقتىدارنى قوللىمايدۇ ، يۇقىرى نەشىردىكى تور كۆرگۈچنى ئىشلىتىڭ',
		success: 'غەلبىلىك بولدى',
		fail: 'مەغلۇب بولدى',
		preview: 'ئۈنۈم رەسىم',
		btn: {
			off: 'بولدى قىلىش',
			close: 'تاقاش',
			back: 'ئالدىنقى قەدەم',
			save: 'ساقلاش'
		},
		error: {
			onlyImg: 'پەقەت رەسىم فورماتىنىلا قوللايدۇ',
			outOfSize: 'رەسىم چوڭ - كىچىكلىكى چەكتىن ئىشىپ كەتتى',
			lowestPx: 'رەسىمنىڭ ئەڭ كىچىك ئۆلچىمى :'
		}
	},
	th: {
		hint: 'คลิ๊กหรือลากรูปมาที่นี่',
		loading: 'กำลังอัพโหลด…',
		noSupported: 'เบราเซอร์ไม่รองรับ, กรุณาใช้ IE เวอร์ชั่น 10 ขึ้นไป หรือใช้เบราเซอร์ตัวอื่น',
		success: 'อัพโหลดสำเร็จ',
		fail: 'อัพโหลดล้มเหลว',
		preview: 'ตัวอย่าง',
		btn: {
			off: 'ยกเลิก',
			close: 'ปิด',
			back: 'กลับ',
			save: 'บันทึก'
		},
		error: {
			onlyImg: 'ไฟล์ภาพเท่านั้น',
			outOfSize: 'ไฟล์ใหญ่เกินกำหนด: ',
			lowestPx: 'ไฟล์เล็กเกินไป. อย่างน้อยต้องมีขนาด: '
		}
	},
	mm: {
		hint: 'ဖိုင်ကို ဤနေရာတွင် နှိပ်၍ (သို့) ဆွဲထည့်၍ တင်ပါ',
		loading: 'တင်နေသည်…',
		noSupported: 'ဤဘရောက်ဇာကို အထောက်အပံ့ မပေးပါ၊ ကျေးဇူးပြု၍ IE10+ သို့မဟုတ် အခြား ဘရောက်ဇာ ကို အသုံးပြုပါ',
		success: 'ဖိုင်တင်နေမှု မပြီးမြောက်ပါ',
		fail: 'ဖိုင်တင်နေမှု မအောင်မြင်ပါ',
		preview: 'အစမ်းကြည့်',
		btn: {
			off: 'မလုပ်တော့ပါ',
			close: 'ပိတ်မည်',
			back: 'နောက်သို့',
			save: 'သိမ်းမည်'
		},
		error: {
			onlyImg: 'ဓာတ်ပုံ သီးသန့်သာ',
			outOfSize: 'ဓာတ်ပုံဆိုဒ် ကြီးလွန်းသည် ။ အများဆုံး ဆိုဒ် : ',
			lowestPx: 'ဓာတ်ပုံဆိုဒ် သေးလွန်းသည်။ အနည်းဆုံး ဆိုဒ် : '
		}
	},
	se: {
		hint: 'Klicka eller dra en fil hit för att ladda upp den',
		loading: 'Laddar upp…',
		noSupported: 'Din webbläsare stöds inte, vänligen använd IE10+ eller andra webbläsare',
		success: 'Uppladdning lyckades',
		fail: 'Uppladdning misslyckades',
		preview: 'Förhandsgranska',
		btn: {
			off: 'Avbryt',
			close: 'Stäng',
			back: 'Tillbaka',
			save: 'Spara'
		},
		error: {
			onlyImg: 'Endast bilder',
			outOfSize: 'Bilden är större än max-gränsen: ',
			lowestPx: 'Bilden är för liten. Minimum är: '
		}
	}
});

// CONCATENATED MODULE: ./node_modules/vue-image-crop-upload/utils/mimes.js
/* harmony default export */ var mimes = ({
    'jpg': 'image/jpeg',
    'png': 'image/png',
    'gif': 'image/gif',
    'svg': 'image/svg+xml',
    'psd': 'image/photoshop'
});

// CONCATENATED MODULE: ./node_modules/vue-image-crop-upload/utils/data2blob.js
/**
 * database64文件格式转换为2进制
 *
 * @param  {[String]} data dataURL 的格式为 “data:image/png;base64,****”,逗号之前都是一些说明性的文字，我们只需要逗号之后的就行了
 * @param  {[String]} mime [description]
 * @return {[blob]}      [description]
 */
/* harmony default export */ var data2blob = (function(data, mime) {
	data = data.split(',')[1];
	data = window.atob(data);
	var ia = new Uint8Array(data.length);
	for (var i = 0; i < data.length; i++) {
		ia[i] = data.charCodeAt(i);
	};
	// canvas.toDataURL 返回的默认格式就是 image/png
	return new Blob([ia], {
		type: mime
	});
});;

// CONCATENATED MODULE: ./node_modules/vue-image-crop-upload/utils/effectRipple.js
/**
 * 点击波纹效果
 *
 * @param  {[event]} e        [description]
 * @param  {[Object]} arg_opts [description]
 * @return {[bollean]}          [description]
 */
/* harmony default export */ var effectRipple = (function(e, arg_opts) {
	var opts = Object.assign({
			ele: e.target, // 波纹作用元素
			type: 'hit', // hit点击位置扩散　center中心点扩展
			bgc: 'rgba(0, 0, 0, 0.15)' // 波纹颜色
		}, arg_opts),
		target = opts.ele;
	if (target) {
		var rect = target.getBoundingClientRect(),
			ripple = target.querySelector('.e-ripple');
		if (!ripple) {
			ripple = document.createElement('span');
			ripple.className = 'e-ripple';
			ripple.style.height = ripple.style.width = Math.max(rect.width, rect.height) + 'px';
			target.appendChild(ripple);
		} else {
			ripple.className = 'e-ripple';
		}
		switch (opts.type) {
			case 'center':
				ripple.style.top = (rect.height / 2 - ripple.offsetHeight / 2) + 'px';
				ripple.style.left = (rect.width / 2 - ripple.offsetWidth / 2) + 'px';
				break;
			default:
				ripple.style.top = (e.pageY - rect.top - ripple.offsetHeight / 2 - document.body.scrollTop) + 'px';
				ripple.style.left = (e.pageX - rect.left - ripple.offsetWidth / 2 - document.body.scrollLeft) + 'px';
		}
		ripple.style.backgroundColor = opts.bgc;
		ripple.className = 'e-ripple z-active';
		return false;
	}
});;

// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js??ref--12-0!./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib!./node_modules/cache-loader/dist/cjs.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./node_modules/vue-image-crop-upload/upload-2.vue?vue&type=script&lang=js&
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//






/* harmony default export */ var upload_2vue_type_script_lang_js_ = ({
  props: {
    // 域，上传文件name，触发事件会带上（如果一个页面多个图片上传控件，可以做区分
    field: {
      type: String,
      'default': 'avatar'
    },
    // 原名key，类似于id，触发事件会带上（如果一个页面多个图片上传控件，可以做区分
    ki: {
      'default': 0
    },
    // 显示该控件与否
    value: {
      'default': true
    },
    // 上传地址
    url: {
      type: String,
      'default': ''
    },
    // 其他要上传文件附带的数据，对象格式
    params: {
      type: Object,
      'default': null
    },
    //Add custom headers
    headers: {
      type: Object,
      'default': null
    },
    // 剪裁图片的宽
    width: {
      type: Number,
      default: 200
    },
    // 剪裁图片的高
    height: {
      type: Number,
      default: 200
    },
    // 不显示旋转功能
    noRotate: {
      type: Boolean,
      default: true
    },
    // 不预览圆形图片
    noCircle: {
      type: Boolean,
      default: false
    },
    // 不预览方形图片
    noSquare: {
      type: Boolean,
      default: false
    },
    // 单文件大小限制
    maxSize: {
      type: Number,
      'default': 10240
    },
    // 语言类型
    langType: {
      type: String,
      'default': 'zh'
    },
    // 语言包
    langExt: {
      type: Object,
      'default': null
    },
    // 图片上传格式
    imgFormat: {
      type: String,
      'default': 'png'
    },
    // 图片背景 jpg情况下生效
    imgBgc: {
      type: String,
      'default': '#fff'
    },
    // 是否支持跨域
    withCredentials: {
      type: Boolean,
      'default': false
    },
    method: {
      type: String,
      'default': 'POST'
    }
  },

  data() {
    let that = this,
        {
      imgFormat,
      langType,
      langExt,
      width,
      height
    } = that,
        isSupported = true,
        allowImgFormat = ['jpg', 'png'],
        tempImgFormat = allowImgFormat.indexOf(imgFormat) === -1 ? 'jpg' : imgFormat,
        lang = language[langType] ? language[langType] : language['en'],
        mime = mimes[tempImgFormat]; // 规范图片格式

    that.imgFormat = tempImgFormat;

    if (langExt) {
      Object.assign(lang, langExt);
    }

    if (typeof FormData != 'function') {
      isSupported = false;
    }

    return {
      // 图片的mime
      mime,
      // 语言包
      lang,
      // 浏览器是否支持该控件
      isSupported,
      // 浏览器是否支持触屏事件
      isSupportTouch: document.hasOwnProperty("ontouchstart"),
      // 步骤
      step: 1,
      //1选择文件 2剪裁 3上传
      // 上传状态及进度
      loading: 0,
      //0未开始 1正在 2成功 3错误
      progress: 0,
      // 是否有错误及错误信息
      hasError: false,
      errorMsg: '',
      // 需求图宽高比
      ratio: width / height,
      // 原图地址、生成图片地址
      sourceImg: null,
      sourceImgUrl: '',
      createImgUrl: '',
      // 原图片拖动事件初始值
      sourceImgMouseDown: {
        on: false,
        mX: 0,
        //鼠标按下的坐标
        mY: 0,
        x: 0,
        //scale原图坐标
        y: 0
      },
      // 生成图片预览的容器大小
      previewContainer: {
        width: 100,
        height: 100
      },
      // 原图容器宽高
      sourceImgContainer: {
        // sic
        width: 240,
        height: 184 // 如果生成图比例与此一致会出现bug，先改成特殊的格式吧，哈哈哈

      },
      // 原图展示属性
      scale: {
        zoomAddOn: false,
        //按钮缩放事件开启
        zoomSubOn: false,
        //按钮缩放事件开启
        range: 1,
        //最大100
        x: 0,
        y: 0,
        width: 0,
        height: 0,
        maxWidth: 0,
        maxHeight: 0,
        minWidth: 0,
        //最宽
        minHeight: 0,
        naturalWidth: 0,
        //原宽
        naturalHeight: 0
      }
    };
  },

  computed: {
    // 进度条样式
    progressStyle() {
      let {
        progress
      } = this;
      return {
        width: progress + '%'
      };
    },

    // 原图样式
    sourceImgStyle() {
      let {
        scale,
        sourceImgMasking
      } = this,
          top = scale.y + sourceImgMasking.y + 'px',
          left = scale.x + sourceImgMasking.x + 'px';
      return {
        top,
        left,
        width: scale.width + 'px',
        height: scale.height + 'px' // 兼容 Opera

      };
    },

    // 原图蒙版属性
    sourceImgMasking() {
      let {
        width,
        height,
        ratio,
        sourceImgContainer
      } = this,
          sic = sourceImgContainer,
          sicRatio = sic.width / sic.height,
          // 原图容器宽高比
      x = 0,
          y = 0,
          w = sic.width,
          h = sic.height,
          scale = 1;

      if (ratio < sicRatio) {
        scale = sic.height / height;
        w = sic.height * ratio;
        x = (sic.width - w) / 2;
      }

      if (ratio > sicRatio) {
        scale = sic.width / width;
        h = sic.width / ratio;
        y = (sic.height - h) / 2;
      }

      return {
        scale,
        // 蒙版相对需求宽高的缩放
        x,
        y,
        width: w,
        height: h
      };
    },

    // 原图遮罩样式
    sourceImgShadeStyle() {
      let {
        sourceImgMasking,
        sourceImgContainer
      } = this,
          sic = sourceImgContainer,
          sim = sourceImgMasking,
          w = sim.width == sic.width ? sim.width : (sic.width - sim.width) / 2,
          h = sim.height == sic.height ? sim.height : (sic.height - sim.height) / 2;
      return {
        width: w + 'px',
        height: h + 'px'
      };
    },

    previewStyle() {
      let {
        width,
        height,
        ratio,
        previewContainer
      } = this,
          pc = previewContainer,
          w = pc.width,
          h = pc.height,
          pcRatio = w / h;

      if (ratio < pcRatio) {
        w = pc.height * ratio;
      }

      if (ratio > pcRatio) {
        h = pc.width / ratio;
      }

      return {
        width: w + 'px',
        height: h + 'px'
      };
    }

  },
  watch: {
    value(newValue) {
      if (newValue && this.loading != 1) {
        this.reset();
      }
    }

  },
  methods: {
    // 点击波纹效果
    ripple(e) {
      effectRipple(e);
    },

    // 关闭控件
    off() {
      setTimeout(() => {
        this.$emit('input', false);

        if (this.step == 3 && this.loading == 2) {
          this.setStep(1);
        }
      }, 200);
    },

    // 设置步骤
    setStep(no) {
      // 延时是为了显示动画效果呢，哈哈哈
      setTimeout(() => {
        this.step = no;
      }, 200);
    },

    /* 图片选择区域函数绑定
     ---------------------------------------------------------------*/
    preventDefault(e) {
      e.preventDefault();
      return false;
    },

    handleClick(e) {
      if (this.loading !== 1) {
        if (e.target !== this.$refs.fileinput) {
          e.preventDefault();

          if (document.activeElement !== this.$refs) {
            this.$refs.fileinput.click();
          }
        }
      }
    },

    handleChange(e) {
      e.preventDefault();

      if (this.loading !== 1) {
        let files = e.target.files || e.dataTransfer.files;
        this.reset();

        if (this.checkFile(files[0])) {
          this.setSourceImg(files[0]);
        }
      }
    },

    /* ---------------------------------------------------------------*/
    // 检测选择的文件是否合适
    checkFile(file) {
      let that = this,
          {
        lang,
        maxSize
      } = that; // 仅限图片

      if (file.type.indexOf('image') === -1) {
        that.hasError = true;
        that.errorMsg = lang.error.onlyImg;
        return false;
      } // 超出大小


      if (file.size / 1024 > maxSize) {
        that.hasError = true;
        that.errorMsg = lang.error.outOfSize + maxSize + 'kb';
        return false;
      }

      return true;
    },

    // 重置控件
    reset() {
      let that = this;
      that.loading = 0;
      that.hasError = false;
      that.errorMsg = '';
      that.progress = 0;
    },

    // 设置图片源
    setSourceImg(file) {
      this.$emit('src-file-set', file.name, file.type, file.size);
      let that = this,
          fr = new FileReader();

      fr.onload = function (e) {
        that.sourceImgUrl = fr.result;
        that.startCrop();
      };

      fr.readAsDataURL(file);
    },

    // 剪裁前准备工作
    startCrop() {
      let that = this,
          {
        width,
        height,
        ratio,
        scale,
        sourceImgUrl,
        sourceImgMasking,
        lang
      } = that,
          sim = sourceImgMasking,
          img = new Image();
      img.src = sourceImgUrl;

      img.onload = function () {
        let nWidth = img.naturalWidth,
            nHeight = img.naturalHeight,
            nRatio = nWidth / nHeight,
            w = sim.width,
            h = sim.height,
            x = 0,
            y = 0; // 图片像素不达标

        if (nWidth < width || nHeight < height) {
          that.hasError = true;
          that.errorMsg = lang.error.lowestPx + width + '*' + height;
          return false;
        }

        if (ratio > nRatio) {
          h = w / nRatio;
          y = (sim.height - h) / 2;
        }

        if (ratio < nRatio) {
          w = h * nRatio;
          x = (sim.width - w) / 2;
        }

        scale.range = 0;
        scale.x = x;
        scale.y = y;
        scale.width = w;
        scale.height = h;
        scale.minWidth = w;
        scale.minHeight = h;
        scale.maxWidth = nWidth * sim.scale;
        scale.maxHeight = nHeight * sim.scale;
        scale.naturalWidth = nWidth;
        scale.naturalHeight = nHeight;
        that.sourceImg = img;
        that.createImg();
        that.setStep(2);
      };
    },

    // 鼠标按下图片准备移动
    imgStartMove(e) {
      e.preventDefault(); // 支持触摸事件，则鼠标事件无效

      if (this.isSupportTouch && !e.targetTouches) {
        return false;
      }

      let et = e.targetTouches ? e.targetTouches[0] : e,
          {
        sourceImgMouseDown,
        scale
      } = this,
          simd = sourceImgMouseDown;
      simd.mX = et.screenX;
      simd.mY = et.screenY;
      simd.x = scale.x;
      simd.y = scale.y;
      simd.on = true;
    },

    // 鼠标按下状态下移动，图片移动
    imgMove(e) {
      e.preventDefault(); // 支持触摸事件，则鼠标事件无效

      if (this.isSupportTouch && !e.targetTouches) {
        return false;
      }

      let et = e.targetTouches ? e.targetTouches[0] : e,
          {
        sourceImgMouseDown: {
          on,
          mX,
          mY,
          x,
          y
        },
        scale,
        sourceImgMasking
      } = this,
          sim = sourceImgMasking,
          nX = et.screenX,
          nY = et.screenY,
          dX = nX - mX,
          dY = nY - mY,
          rX = x + dX,
          rY = y + dY;
      if (!on) return;

      if (rX > 0) {
        rX = 0;
      }

      if (rY > 0) {
        rY = 0;
      }

      if (rX < sim.width - scale.width) {
        rX = sim.width - scale.width;
      }

      if (rY < sim.height - scale.height) {
        rY = sim.height - scale.height;
      }

      scale.x = rX;
      scale.y = rY;
    },

    // 顺时针旋转图片
    rotateImg(e) {
      let {
        sourceImg,
        scale: {
          naturalWidth,
          naturalHeight
        }
      } = this,
          width = naturalHeight,
          height = naturalWidth,
          canvas = this.$refs.canvas,
          ctx = canvas.getContext('2d');
      canvas.width = width;
      canvas.height = height;
      ctx.clearRect(0, 0, width, height);
      ctx.fillStyle = 'rgba(0,0,0,0)';
      ctx.fillRect(0, 0, width, height);
      ctx.translate(width, 0);
      ctx.rotate(Math.PI * 90 / 180);
      ctx.drawImage(sourceImg, 0, 0, naturalWidth, naturalHeight);
      let imgUrl = canvas.toDataURL(mimes['png']);
      this.sourceImgUrl = imgUrl;
      this.startCrop();
    },

    // 按钮按下开始放大
    startZoomAdd(e) {
      let that = this,
          {
        scale
      } = that;
      scale.zoomAddOn = true;

      function zoom() {
        if (scale.zoomAddOn) {
          let range = scale.range >= 100 ? 100 : ++scale.range;
          that.zoomImg(range);
          setTimeout(function () {
            zoom();
          }, 60);
        }
      }

      zoom();
    },

    // 按钮松开或移开取消放大
    endZoomAdd(e) {
      this.scale.zoomAddOn = false;
    },

    // 按钮按下开始缩小
    startZoomSub(e) {
      let that = this,
          {
        scale
      } = that;
      scale.zoomSubOn = true;

      function zoom() {
        if (scale.zoomSubOn) {
          let range = scale.range <= 0 ? 0 : --scale.range;
          that.zoomImg(range);
          setTimeout(function () {
            zoom();
          }, 60);
        }
      }

      zoom();
    },

    // 按钮松开或移开取消缩小
    endZoomSub(e) {
      let {
        scale
      } = this;
      scale.zoomSubOn = false;
    },

    zoomChange(e) {
      this.zoomImg(e.target.value);
    },

    // 缩放原图
    zoomImg(newRange) {
      let that = this,
          {
        sourceImgMasking,
        sourceImgMouseDown,
        scale
      } = this,
          {
        maxWidth,
        maxHeight,
        minWidth,
        minHeight,
        width,
        height,
        x,
        y,
        range
      } = scale,
          sim = sourceImgMasking,
          // 蒙版宽高
      sWidth = sim.width,
          sHeight = sim.height,
          // 新宽高
      nWidth = minWidth + (maxWidth - minWidth) * newRange / 100,
          nHeight = minHeight + (maxHeight - minHeight) * newRange / 100,
          // 新坐标（根据蒙版中心点缩放）
      nX = sWidth / 2 - nWidth / width * (sWidth / 2 - x),
          nY = sHeight / 2 - nHeight / height * (sHeight / 2 - y); // 判断新坐标是否超过蒙版限制

      if (nX > 0) {
        nX = 0;
      }

      if (nY > 0) {
        nY = 0;
      }

      if (nX < sWidth - nWidth) {
        nX = sWidth - nWidth;
      }

      if (nY < sHeight - nHeight) {
        nY = sHeight - nHeight;
      } // 赋值处理


      scale.x = nX;
      scale.y = nY;
      scale.width = nWidth;
      scale.height = nHeight;
      scale.range = newRange;
      setTimeout(function () {
        if (scale.range == newRange) {
          that.createImg();
        }
      }, 300);
    },

    // 生成需求图片
    createImg(e) {
      let that = this,
          {
        imgFormat,
        imgBgc,
        mime,
        sourceImg,
        scale: {
          x,
          y,
          width,
          height
        },
        sourceImgMasking: {
          scale
        }
      } = that,
          canvas = that.$refs.canvas,
          ctx = canvas.getContext('2d');

      if (e) {
        // 取消鼠标按下移动状态
        that.sourceImgMouseDown.on = false;
      }

      canvas.width = that.width;
      canvas.height = that.height;
      ctx.clearRect(0, 0, that.width, that.height);

      if (imgFormat == 'png') {
        ctx.fillStyle = 'rgba(0,0,0,0)';
      } else {
        // 如果jpg 为透明区域设置背景，默认白色
        ctx.fillStyle = imgBgc;
      }

      ctx.fillRect(0, 0, that.width, that.height);
      ctx.drawImage(sourceImg, x / scale, y / scale, width / scale, height / scale);
      that.createImgUrl = canvas.toDataURL(mime);
    },

    prepareUpload() {
      let {
        url,
        createImgUrl,
        field,
        ki
      } = this;
      this.$emit('crop-success', createImgUrl, field, ki);

      if (typeof url == 'string' && url) {
        this.upload();
      } else {
        this.off();
      }
    },

    // 上传图片
    upload() {
      let that = this,
          {
        lang,
        imgFormat,
        mime,
        url,
        params,
        headers,
        field,
        ki,
        createImgUrl,
        withCredentials,
        method
      } = this,
          fmData = new FormData();
      fmData.append(field, data2blob(createImgUrl, mime), field + '.' + imgFormat); // 添加其他参数

      if (typeof params == 'object' && params) {
        Object.keys(params).forEach(k => {
          fmData.append(k, params[k]);
        });
      } // 监听进度回调


      const uploadProgress = function (event) {
        if (event.lengthComputable) {
          that.progress = 100 * Math.round(event.loaded) / event.total;
        }
      }; // 上传文件


      that.reset();
      that.loading = 1;
      that.setStep(3);
      new Promise(function (resolve, reject) {
        let client = new XMLHttpRequest();
        client.open(method, url, true);
        client.withCredentials = withCredentials;

        client.onreadystatechange = function () {
          if (this.readyState !== 4) {
            return;
          }

          if (this.status === 200 || this.status === 201) {
            resolve(JSON.parse(this.responseText));
          } else {
            reject(this.status);
          }
        };

        client.upload.addEventListener("progress", uploadProgress, false); //监听进度
        // 设置header

        if (typeof headers == 'object' && headers) {
          Object.keys(headers).forEach(k => {
            client.setRequestHeader(k, headers[k]);
          });
        }

        client.send(fmData);
      }).then( // 上传成功
      function (resData) {
        if (that.value) {
          that.loading = 2;
          that.$emit('crop-upload-success', resData, field, ki);
        }
      }, // 上传失败
      function (sts) {
        if (that.value) {
          that.loading = 3;
          that.hasError = true;
          that.errorMsg = lang.fail;
          that.$emit('crop-upload-fail', sts, field, ki);
        }
      });
    }

  },

  created() {
    // 绑定按键esc隐藏此插件事件
    document.addEventListener('keyup', e => {
      if (this.value && (e.key == 'Escape' || e.keyCode == 27)) {
        this.off();
      }
    });
  }

});
// CONCATENATED MODULE: ./node_modules/vue-image-crop-upload/upload-2.vue?vue&type=script&lang=js&
 /* harmony default export */ var vue_image_crop_upload_upload_2vue_type_script_lang_js_ = (upload_2vue_type_script_lang_js_); 
// EXTERNAL MODULE: ./node_modules/vue-image-crop-upload/upload-2.vue?vue&type=style&index=0&lang=css&
var upload_2vue_type_style_index_0_lang_css_ = __webpack_require__("2133");

// CONCATENATED MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
/* globals __VUE_SSR_CONTEXT__ */

// IMPORTANT: Do NOT use ES2015 features in this file (except for modules).
// This module is a runtime utility for cleaner component module output and will
// be included in the final webpack user bundle.

function normalizeComponent (
  scriptExports,
  render,
  staticRenderFns,
  functionalTemplate,
  injectStyles,
  scopeId,
  moduleIdentifier, /* server only */
  shadowMode /* vue-cli only */
) {
  // Vue.extend constructor export interop
  var options = typeof scriptExports === 'function'
    ? scriptExports.options
    : scriptExports

  // render functions
  if (render) {
    options.render = render
    options.staticRenderFns = staticRenderFns
    options._compiled = true
  }

  // functional template
  if (functionalTemplate) {
    options.functional = true
  }

  // scopedId
  if (scopeId) {
    options._scopeId = 'data-v-' + scopeId
  }

  var hook
  if (moduleIdentifier) { // server build
    hook = function (context) {
      // 2.3 injection
      context =
        context || // cached call
        (this.$vnode && this.$vnode.ssrContext) || // stateful
        (this.parent && this.parent.$vnode && this.parent.$vnode.ssrContext) // functional
      // 2.2 with runInNewContext: true
      if (!context && typeof __VUE_SSR_CONTEXT__ !== 'undefined') {
        context = __VUE_SSR_CONTEXT__
      }
      // inject component styles
      if (injectStyles) {
        injectStyles.call(this, context)
      }
      // register component module identifier for async chunk inferrence
      if (context && context._registeredComponents) {
        context._registeredComponents.add(moduleIdentifier)
      }
    }
    // used by ssr in case component is cached and beforeCreate
    // never gets called
    options._ssrRegister = hook
  } else if (injectStyles) {
    hook = shadowMode
      ? function () {
        injectStyles.call(
          this,
          (options.functional ? this.parent : this).$root.$options.shadowRoot
        )
      }
      : injectStyles
  }

  if (hook) {
    if (options.functional) {
      // for template-only hot-reload because in that case the render fn doesn't
      // go through the normalizer
      options._injectStyles = hook
      // register for functional component in vue file
      var originalRender = options.render
      options.render = function renderWithStyleInjection (h, context) {
        hook.call(context)
        return originalRender(h, context)
      }
    } else {
      // inject component registration as beforeCreate hook
      var existing = options.beforeCreate
      options.beforeCreate = existing
        ? [].concat(existing, hook)
        : [hook]
    }
  }

  return {
    exports: scriptExports,
    options: options
  }
}

// CONCATENATED MODULE: ./node_modules/vue-image-crop-upload/upload-2.vue






/* normalize component */

var component = normalizeComponent(
  vue_image_crop_upload_upload_2vue_type_script_lang_js_,
  upload_2vue_type_template_id_258c767c_render,
  upload_2vue_type_template_id_258c767c_staticRenderFns,
  false,
  null,
  null,
  null
  
)

/* harmony default export */ var upload_2 = (component.exports);
// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js?{"cacheDirectory":"node_modules/.cache/vue-loader","cacheIdentifier":"3d2f78f8-vue-loader-template"}!./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/cache-loader/dist/cjs.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./node_modules/vue-ele-gallery/lib/EleGallery.vue?vue&type=template&id=5268d882&
var EleGalleryvue_type_template_id_5268d882_render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return (_vm.source)?_c('div',{staticClass:"ele-gallery"},[_c('div',{staticClass:"el-upload-list el-upload-list--picture-card"},_vm._l((_vm.computedSources),function(source,index){return _c('li',{key:index,staticClass:"el-upload-list__item"},[_vm._t("default",function(){return [(source.type === 'image')?_c('el-image',{style:(_vm.computedStyle),attrs:{"lazy":_vm.lazy,"src":source.thumb,"fit":"cover"}}):(source.type === 'video')?_c('video',{style:(_vm.computedStyle),attrs:{"src":source.thumb}}):(source.type === 'iframe')?_c('div',{staticClass:"ele-gallery-iframe embed-responsive embed-responsive-16by9",style:(_vm.computedStyle)},[_c('iframe',{attrs:{"src":source.thumb,"allowfullscreen":"true","border":"0","frameborder":"no","framespacing":"0","scrolling":"no"}})]):_vm._e()]},null,{source: source, index: index}),_c('span',{staticClass:"el-upload-list__item-actions"},[_c('span',{on:{"click":function($event){return _vm.handlePreview(index, source)}}},[_c('i',{class:_vm.viewClass})]),(_vm.removeFn)?_c('span',{on:{"click":function($event){return _vm.handleRemove(index)}}},[_c('i',{staticClass:"el-icon-delete"})]):_vm._e(),_vm._t("action",null,null,{source: source, index: index})],2)],2)}),0),_c('ele-gallery-dialog',{ref:"dialog",attrs:{"carouselAttrs":_vm.carouselAttrs,"sliceSingle":_vm.sliceSingle,"sources":_vm.computedSources,"title":_vm.title,"type":_vm.type}})],1):_vm._e()}
var EleGalleryvue_type_template_id_5268d882_staticRenderFns = []


// CONCATENATED MODULE: ./node_modules/vue-ele-gallery/lib/EleGallery.vue?vue&type=template&id=5268d882&

// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js?{"cacheDirectory":"node_modules/.cache/vue-loader","cacheIdentifier":"3d2f78f8-vue-loader-template"}!./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/cache-loader/dist/cjs.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./node_modules/vue-ele-gallery/lib/components/EleGalleryDialog.vue?vue&type=template&id=bfa03e68&
var EleGalleryDialogvue_type_template_id_bfa03e68_render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('el-dialog',{staticStyle:{"text-align":"center"},attrs:{"visible":_vm.isShowPreview,"append-to-body":""},on:{"update:visible":function($event){_vm.isShowPreview=$event},"open":function($event){_vm.isShowCarousel = true},"closed":function($event){_vm.isShowCarousel = false}}},[_c('div',{attrs:{"slot":"title"},slot:"title"},[_vm._v(_vm._s(_vm.sourceTitle || _vm.title))]),(_vm.type === 'video')?[(_vm.isShowPreview)?_c('video',{staticClass:"ele-gallery-video",attrs:{"src":_vm.sources[_vm.initialIndex].src,"autoplay":"autoplay","controls":"controls","width":"100%"}}):_vm._e()]:(_vm.type === 'iframe')?[(_vm.isShowPreview)?_c('div',{staticClass:"ele-gallery-iframe embed-responsive embed-responsive-16by9"},[_c('iframe',{attrs:{"src":_vm.sources[_vm.initialIndex].src,"allowfullscreen":"true","border":"0","frameborder":"no","framespacing":"0","scrolling":"no"}})]):_vm._e()]:(_vm.type === 'image')?[((_vm.sliceSingle && _vm.sources[_vm.initialIndex]) || _vm.sources.length === 1)?_c('img',{staticClass:"ele-gallery-image",attrs:{"src":_vm.sources[_vm.initialIndex].src}}):[(_vm.isShowCarousel)?_c('el-carousel',_vm._b({attrs:{"initial-index":_vm.initialIndex,"indicator-position":"outside"},on:{"change":_vm.handleCarouselChange}},'el-carousel',_vm.carouselAttrs,false),_vm._l((_vm.sources),function(image,index){return _c('el-carousel-item',{key:index},[_c('img',{staticClass:"ele-gallery-image",attrs:{"src":image.src}})])}),1):_vm._e()]]:_vm._e()],2)}
var EleGalleryDialogvue_type_template_id_bfa03e68_staticRenderFns = []


// CONCATENATED MODULE: ./node_modules/vue-ele-gallery/lib/components/EleGalleryDialog.vue?vue&type=template&id=bfa03e68&

// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js??ref--12-0!./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib!./node_modules/cache-loader/dist/cjs.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./node_modules/vue-ele-gallery/lib/components/EleGalleryDialog.vue?vue&type=script&lang=js&
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ var EleGalleryDialogvue_type_script_lang_js_ = ({
  name: 'ele-gallery-dialog',
  props: {
    type: String,
    title: String,
    sources: Array,
    sliceSingle: {
      type: Boolean,
      default: false
    },
    carouselAttrs: Object
  },

  data() {
    return {
      isShowCarousel: false,
      sourceTitle: '',
      initialIndex: 0,
      isShowPreview: false
    };
  },

  methods: {
    startPreview(index) {
      this.isShowPreview = true;
      this.initialIndex = index;
      this.sourceTitle = this.sources[index].title;
    },

    handleCarouselChange(index) {
      if (this.sources[index] && this.sources[index].title) {
        this.sourceTitle = this.sources[index].title;
      } else {
        this.sourceTitle = '';
      }
    }

  }
});
// CONCATENATED MODULE: ./node_modules/vue-ele-gallery/lib/components/EleGalleryDialog.vue?vue&type=script&lang=js&
 /* harmony default export */ var components_EleGalleryDialogvue_type_script_lang_js_ = (EleGalleryDialogvue_type_script_lang_js_); 
// CONCATENATED MODULE: ./node_modules/vue-ele-gallery/lib/components/EleGalleryDialog.vue





/* normalize component */

var EleGalleryDialog_component = normalizeComponent(
  components_EleGalleryDialogvue_type_script_lang_js_,
  EleGalleryDialogvue_type_template_id_bfa03e68_render,
  EleGalleryDialogvue_type_template_id_bfa03e68_staticRenderFns,
  false,
  null,
  null,
  null
  
)

/* harmony default export */ var EleGalleryDialog = (EleGalleryDialog_component.exports);
// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js??ref--12-0!./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib!./node_modules/cache-loader/dist/cjs.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./node_modules/vue-ele-gallery/lib/EleGallery.vue?vue&type=script&lang=js&
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ var EleGalleryvue_type_script_lang_js_ = ({
  name: 'ele-gallery',
  props: {
    // 类型(支持图片, 视频, iframe)
    type: {
      type: String,
      default: 'image',

      validator(value) {
        return ['image', 'video', 'iframe'].includes(value);
      }

    },
    // 缩略图大小, 宽 === 高时, 简略写法
    size: Number,
    // 缩略图宽度, 当给定width时, 会覆盖size的值
    width: Number,
    // 缩略图高度, 当给定height时, 会覆盖size值
    height: Number,
    // 缩略图是否懒加载
    lazy: {
      type: Boolean,
      default: false
    },
    // 源
    source: [String, Array, Object],
    // 缩略图后缀
    // 当type为image时, 且未指定thumb, 可通过thumbSuffix设置缩略图
    thumbSuffix: String,
    // 缩略图样式
    thumbStyle: Object,
    // 轮播图属性
    carouselAttrs: Object,
    // 删除函数
    removeFn: Function,
    // 统一的弹框标题
    title: String,
    // 强制多张图片按照单张显示
    sliceSingle: {
      type: Boolean,
      default: false
    }
  },
  components: {
    EleGalleryDialog: EleGalleryDialog
  },
  computed: {
    viewClass() {
      if (this.type === 'video' || this.type === 'iframe') {
        return 'el-icon-video-play';
      } else {
        return 'el-icon-zoom-in';
      }
    },

    computedStyle() {
      let width = this.width || this.size;
      let height = this.height || this.size;

      if (this.type === 'image') {
        width = width ? width + 'px' : '150px';
        height = height ? height + 'px' : '150px';
      } else if (this.type === 'video') {
        width = width ? width + 'px' : '360px';
        height = height ? height + 'px' : 'auto';
      } else if (this.type === 'iframe') {
        width = width ? width + 'px' : '360px';
        height = height ? height + 'px' : 'auto';
      }

      return Object.assign({}, {
        width,
        height,
        display: 'block'
      }, this.thumbStyle);
    },

    // 缩略图
    thumbs() {
      return this.computedSources.map(item => {
        return item.thumb;
      });
    },

    computedSources() {
      const sources = this.source;

      if (typeof sources === 'string') {
        // 传入参数为 string
        return [this.getStringSource(sources)];
      } else if (sources instanceof Array) {
        // 传入参数为 array, 数据里面既可以有string 又可以有 object
        const res = [];
        sources.forEach(item => {
          if (item instanceof Object) {
            res.push(this.getObjectSource(item));
          } else if (typeof item === 'string') {
            res.push(this.getStringSource(item));
          } else {
            console.warn('数组元素错误', sources, item);
          }
        });
        return res;
      } else if (sources instanceof Object) {
        // 传入参数为 object
        return [this.getObjectSource(sources)];
      } else {
        return [];
      }
    }

  },
  methods: {
    // 点击查看
    handlePreview(index) {
      this.$refs.dialog.startPreview(index);
    },

    handleRemove(index) {
      this.removeFn(index);
    },

    // 获取字符串形式来源
    getStringSource(src) {
      let thumb = src;

      if (this.type === 'image' && this.thumbSuffix) {
        thumb += this.thumbSuffix;
      }

      return {
        type: this.type,
        src: src,
        thumb: thumb
      };
    },

    // 获取对象形式来源
    getObjectSource(source) {
      source.type = source.thumb ? 'image' : this.type;
      source.thumb = source.thumb || source.src;
      return source;
    }

  }
});
// CONCATENATED MODULE: ./node_modules/vue-ele-gallery/lib/EleGallery.vue?vue&type=script&lang=js&
 /* harmony default export */ var lib_EleGalleryvue_type_script_lang_js_ = (EleGalleryvue_type_script_lang_js_); 
// EXTERNAL MODULE: ./node_modules/vue-ele-gallery/lib/EleGallery.vue?vue&type=style&index=0&lang=css&
var EleGalleryvue_type_style_index_0_lang_css_ = __webpack_require__("b289");

// CONCATENATED MODULE: ./node_modules/vue-ele-gallery/lib/EleGallery.vue






/* normalize component */

var EleGallery_component = normalizeComponent(
  lib_EleGalleryvue_type_script_lang_js_,
  EleGalleryvue_type_template_id_5268d882_render,
  EleGalleryvue_type_template_id_5268d882_staticRenderFns,
  false,
  null,
  null,
  null
  
)

/* harmony default export */ var EleGallery = (EleGallery_component.exports);
// CONCATENATED MODULE: ./node_modules/vue-ele-gallery/lib/index.js


if (typeof window !== 'undefined' && window.Vue) {
  window.Vue.component('ele-gallery', EleGallery)
}

/* harmony default export */ var lib = (EleGallery);

// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js??ref--12-0!./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib!./node_modules/cache-loader/dist/cjs.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./lib/EleUploadImage.vue?vue&type=script&lang=js&
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ var EleUploadImagevue_type_script_lang_js_ = ({
  name: "EleUploadImage",
  props: {
    // 值
    value: {
      type: [String, Array],

      default() {
        return [];
      }

    },
    // 是否剪裁
    crop: {
      type: Boolean,
      default: false
    },
    // 图片显示大小
    size: {
      type: Number,
      default: 150
    },
    // 裁剪高度
    cropHeight: {
      type: Number
    },
    // 裁剪宽度
    cropWidth: {
      type: Number
    },
    // 大小限制(MB)
    fileSize: {
      type: Number
    },
    // 响应处理函数
    responseFn: Function,
    // 文件类型, 例如['png', 'jpg', 'jpeg']
    fileType: {
      type: Array,
      default: () => []
    },
    // 是否显示上传成功的提示
    isShowSuccessTip: {
      type: Boolean,
      default: true
    },
    // 缩略图后缀, 例如七牛云缩略图样式 (?imageView2/1/w/20/h/20)
    thumbSuffix: {
      type: String,
      default: ""
    },
    // 是否显示提示
    isShowTip: {
      type: Boolean,
      default: true
    },
    // 弹窗标题
    title: String,
    // 图片懒加载
    lazy: {
      type: Boolean,
      default: false
    },
    // 上传地址 (同官网)
    action: {
      type: String,
      required: true
    },
    // 设置上传的请求头部(同官网)
    headers: Object,
    // 文件个数显示(同官网)
    limit: Number,
    // 是否启用拖拽上传 (同官网)
    drag: {
      type: Boolean,
      default: false
    },
    // 	支持发送 cookie 凭证信息 (同官网)
    withCredentials: {
      type: Boolean,
      default: false
    },
    // 是否支持多选文件 (同官网)
    multiple: {
      type: Boolean,
      default: false
    },
    // 上传时附带的额外参数(同官网)
    data: Object,
    // 上传的文件字段名 (同官网)
    name: {
      type: String,
      default: "file"
    },
    // 覆盖默认的上传行为，可以自定义上传的实现 (同官网)
    httpRequest: Function,
    // 接受上传的文件类型（thumbnail-mode 模式下此参数无效）(同官网)
    accept: String,
    // 删除前的操作
    beforeRemove: Function
  },
  components: {
    Cropper: upload_2,
    EleGallery: lib
  },

  data() {
    return {
      cropData: {},
      isShowCrop: false,
      uploading: false,
      fileList: []
    };
  },

  computed: {
    // 是否显示提示
    showTip() {
      return this.isShowTip && (this.fileType.length || this.fileSize);
    },

    computedValues() {
      if (this.value) {
        if (typeof this.value === "string") {
          return [this.value];
        } else {
          return [...this.value];
        }
      } else {
        return [];
      }
    },

    isShowUpload() {
      if (this.multiple) {
        return true;
      } else {
        return this.computedValues.length === 0;
      }
    },

    successFiles() {
      return this.fileList.filter(file => file.status === "success");
    }

  },
  watch: {
    isShowCrop(value) {
      if (value === false) {
        this.cropData = {};
      }
    }

  },
  methods: {
    handleSetFileSet(fileName, fileType, fileSize) {
      const uid = this.cropData.uid || new Date().getTime();
      this.cropData = {
        name: fileName,
        percentage: 0,
        size: fileSize,
        type: fileType,
        status: "ready",
        uid: uid
      };
    },

    handleCropSuccess(b64Data) {
      this.cropData.url = b64Data;
    },

    handleCropUploadError(status) {
      this.$message.error("上传失败, 请重试");
      this.$emit("error", status);
    },

    handleCropUploadSuccess(response) {
      this.cropData.status = "success";
      this.cropData.percentage = 100;
      this.cropData.response = response;
      const file = Object.assign({}, this.cropData);
      let index = this.fileList.findIndex(item => item.uid === file.uid);

      if (index > -1) {
        this.fileList.splice(index, 1, file);
      } else {
        this.fileList.push(file);
      }

      this.handleUploadSuccess(response, file, this.fileList);
    },

    // 上传前校检格式和大小
    handleBeforeUpload(file) {
      let isImg = false;

      if (this.fileType.length) {
        let fileExtension = "";

        if (file.name.lastIndexOf(".") > -1) {
          fileExtension = file.name.slice(file.name.lastIndexOf(".") + 1);
        }

        isImg = this.fileType.some(type => {
          if (file.type.indexOf(type) > -1) return true;
          if (fileExtension && fileExtension.indexOf(type) > -1) return true;
          return false;
        });
      } else {
        isImg = file.type.indexOf("image") > -1;
      }

      if (!isImg) {
        this.$message.error(`文件格式不正确, 请上传${this.fileType.join("/")}图片格式文件!`);
        return false;
      }

      if (this.fileSize) {
        const isLt = file.size / 1024 / 1024 < this.fileSize;

        if (!isLt) {
          this.$message.error(`上传头像图片大小不能超过 ${this.fileSize} MB!`);
          return false;
        }
      }

      this.uploading = true;
      return true;
    },

    handleChange(file, fileList) {
      this.uploading = false;
      this.fileList = fileList;
    },

    // 文件个数超出
    handleExceed() {
      this.$message.error(`最多上传${this.limit}张图片`);
    },

    // 上传失败
    handleUploadError(err) {
      this.uploading = false;
      this.$message.error("上传失败, 请重试");
      this.$emit("error", err);
    },

    // 上传成功回调
    handleUploadSuccess(response, file) {
      if (response.code !== 200) {
        this.$message.error(response.message);
        return false;
      }

      let url = response;
      this.uploading = false;

      if (this.isShowSuccessTip) {
        this.$message.success("上传成功");
      }

      if (this.responseFn) {
        url = this.responseFn(response, file, this.successFiles);
      }

      if (this.multiple) {
        if (Array.isArray(this.value)) {
          this.$emit("input", [...this.value, url]);
        } else {
          this.$emit("input", [url]);
        }
      } else {
        this.$emit("input", url);
      }
    },

    doRemove(index) {
      if (this.multiple) {
        const data = [...this.computedValues];
        data.splice(index, 1);
        this.$emit("input", data || []);
      } else {
        this.$emit("input", null);
      }
    },

    handleRemove(index) {
      if (!this.beforeRemove) {
        this.doRemove(index);
      } else if (typeof this.beforeRemove === "function") {
        const file = this.multiple ? this.computedValues[index] : this.computedValues;
        const before = this.beforeRemove(file, this.computedValues);

        if (before && before.then) {
          before.then(() => {
            this.doRemove(index);
          }, () => {});
        } else if (before !== false) {
          this.doRemove(index);
        }
      }
    }

  },

  mounted() {
    // 插入到body中, 避免弹出层被遮盖
    if (this.crop && this.$refs.cropper) {
      document.body.appendChild(this.$refs.cropper.$el);
    }
  }

});
// CONCATENATED MODULE: ./lib/EleUploadImage.vue?vue&type=script&lang=js&
 /* harmony default export */ var lib_EleUploadImagevue_type_script_lang_js_ = (EleUploadImagevue_type_script_lang_js_); 
// EXTERNAL MODULE: ./lib/EleUploadImage.vue?vue&type=style&index=0&lang=css&
var EleUploadImagevue_type_style_index_0_lang_css_ = __webpack_require__("87dd");

// CONCATENATED MODULE: ./lib/EleUploadImage.vue






/* normalize component */

var EleUploadImage_component = normalizeComponent(
  lib_EleUploadImagevue_type_script_lang_js_,
  render,
  staticRenderFns,
  false,
  null,
  null,
  null
  
)

/* harmony default export */ var EleUploadImage = (EleUploadImage_component.exports);
// CONCATENATED MODULE: ./lib/index.js
/**
 * @Author: Wang chunsheng  email:2192138785@qq.com
 * @Date:   2021-11-27 18:06:12
 * @Last Modified by:   Wang chunsheng  email:2192138785@qq.com
 * @Last Modified time: 2021-11-28 01:25:07
 */


if (typeof window !== 'undefined' && window.Vue) {
  window.Vue.component(EleUploadImage.name, EleUploadImage);
}

/* harmony default export */ var lib_0 = (EleUploadImage);
// CONCATENATED MODULE: ./node_modules/@vue/cli-service/lib/commands/build/entry-lib.js


/* harmony default export */ var entry_lib = __webpack_exports__["default"] = (lib_0);



/***/ })

/******/ });
});
//# sourceMappingURL=diandi-ele-upload-image.umd.js.map